use nautica::{gmi, html, markdown};

fn main() {
    let doc = markdown::parse("# my markdown").unwrap();
    let gmi = gmi::render(doc.clone(), &vec![]).unwrap();
    let html = html::render(doc, &vec![]).unwrap();
    assert_eq!(gmi, "# my markdown\n");
    assert_eq!(html, "<article><h1>my markdown</h1></article>");
}
