use nautica::{gmi, markdown};

fn main() {
    let md = markdown::render(gmi::parse("# a gemtext document").unwrap(), &vec![]).unwrap();
    assert_eq!(md, "# a gemtext document\n");
}
