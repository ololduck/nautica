# Ṇautica

![A harbor under heavy cloud cover](resources/nautica.jpg)

Ṇautica is a parser. She transforms markdown and [gemtext] into a tree of tokens. This can then be used to output html,
gemtext, or markdown.

At this time, she can handle these input formats:

- [x] [gemtext]
- [x] markdown

And output these:

- [x] gemtext
- [x] markdown
- [x] html5

## Usage

### Cargo.toml

```toml
[dependencies]
nautica = { git = "https://framagit.org/ololduck/nautica.git", features = ["serde"] }
```

### Code

All parsers are in their own rust submodule, with a `render` & a `parse` method.

```rust
use nautica::{gmi, markdown};

fn main() {
    let md = markdown::render(gmi::parse("# a gemtext document").unwrap(), &vec![]).unwrap();
    assert_eq!(md, "# a gemtext document\n");
}
```

If you want to render a markdown file both to gmi & html:
```rust
use nautica::{gmi, html, markdown};

fn main() {
    let doc = markdown::parse("# my markdown").unwrap();
    let gmi = gmi::render(doc.clone(), &vec![]).unwrap();
    let html = html::render(doc, &vec![]).unwrap();
    assert_eq!(gmi, "# my markdown\n");
    assert_eq!(html, "<article><h1>my markdown</h1></article>");
}
```

You may have noticed that the `render` functions always have a second parameter: this is to give it a WikiLinks array so it may know what to do with them. See `nautica::WikiLink` in [document.rs](src/document.rs).

## TODO

- [ ] Handle deported links, such as the `[gemtext]` in this README.

[gemtext]: https://gemini.circumlunar.space/docs/gemtext.gmi