use nautica::gmi::render as render_to_gmi;
use nautica::html::render as render_to_html;
use nautica::markdown::parse;
use pretty_assertions::assert_eq;
use pretty_env_logger::try_init;

const MD: &str = r####"# title

This is a paragraph, with a [link](https://link/).

This is a paragraph
cut in two.

```rust
println!("hello world");
```

* this is a list
* with two items
"####;

const GMI: &str = r##"# title

This is a paragraph, with a link[1].

=> https://link/ [1]

This is a paragraph cut in two.

```rust
println!("hello world");
```

* this is a list
* with two items
"##;

const HTML: &str = r##"<article><h1>title</h1><p>This is a paragraph, with a <a href="https://link/">link</a>.</p><p>This is a paragraph cut in two.</p><pre>println!("hello world");
</pre><ul><li>this is a list</li><li>with two items</li></ul></article>"##;

#[test]
fn to_gmi() {
    let _ = try_init();

    let d = parse(MD);
    assert!(d.is_ok());
    let d = d.unwrap();
    let s = render_to_gmi(d, &Vec::new());
    assert!(s.is_ok());
    let s = s.unwrap();
    assert_eq!(GMI, s);
}

#[test]
fn to_html() {
    let _ = try_init();
    let d = parse(MD);
    assert!(d.is_ok());
    let d = d.unwrap();
    let s = render_to_html(d, &Vec::new());
    assert!(s.is_ok());
    let s = s.unwrap();
    println!("{}", s);
    assert_eq!(HTML, s);
}
