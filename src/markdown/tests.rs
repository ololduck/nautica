use crate::markdown::parse;
use crate::markdown::render;
use pretty_assertions::assert_eq;
use pretty_env_logger::try_init;
use test_case::test_case;

#[test]
fn complex_paragraph() {
    let _ = try_init();
    let d = parse("Hello, my name is *paul*, i am __pleased__ to meet you. Here's my `website`: [goélands](http://goelands.vit.am)");
    assert!(d.is_ok());
    let d = d.unwrap();
    let res = render(d, &vec![]);
    assert!(res.is_ok());
    let r = res.unwrap();
    assert_eq!(r, "Hello, my name is _paul_, i am **pleased** to meet you. Here's my `website`: [goélands](http://goelands.vit.am)\n");
}

#[test]
fn list() {
    let _ = try_init();
    let s = r##"* elem 1
* elem 2
* [elem 3](https://goelands.vit.am/)
* `elem 4`
"##;
    let d = parse(s);
    assert!(d.is_ok());
    let d = d.unwrap();
    let r = render(d, &vec![]);
    assert!(r.is_ok());
    let r = r.unwrap();
    assert_eq!(r, s);
}

#[test]
#[ignore]
fn deported_link() {
    let _ = try_init();
    let s = r##"[hello]


[hello]: https://hello.world/
"##;
    let d = parse(s);
    assert!(d.is_ok());
    let d = d.unwrap();
    let r = render(d, &vec![]);
    assert!(r.is_ok());
    let r = r.unwrap();
    assert_eq!(r, "[hello](https://hello.world/");
}

#[test_case("![image desc](lien.png)\n"; "image block")]
#[test_case("a code `span`\n"; "code span")]
#[test_case("_hello_\n"; "emph")]
#[test_case("**hello**\n"; "strong")]
#[test_case("[elem 3](https://goelands.vit.am/)\n"; "link")]
#[test_case("# header\n"; "header")]
#[test_case(
"# Hello

This is a sample paragraph
"; "header plus paragraph")]
#[test_case(
"* hi
* i'm a
* list
"; "list")]
fn parse_and_render(s: &str) {
    let d = parse(s);
    assert!(d.is_ok());
    let d = d.unwrap();
    let r = render(d, &vec![]);
    assert!(r.is_ok());
    let r = r.unwrap();
    self::assert_eq!(r, s);
}
