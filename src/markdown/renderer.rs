use std::fmt::Write;

use crate::utils::get_wikilink;
use crate::List::UnOrdered;
use crate::{Block, Document, List, RenderError, Span, Text, WikiLinks};

#[allow(unused_must_use)]
fn render_span(span: Span, wikilinks: &WikiLinks) -> Result<String, RenderError> {
    let mut s = String::new();
    match span {
        Span::Text(t) => match t {
            Text::Plain(str) => {
                write!(s, "{}", str);
            }
            Text::Tree(t) => {
                for t in t {
                    write!(s, "{}", render_span(t, &WikiLinks::new())?);
                }
            }
        },
        Span::Bold(t) => {
            write!(s, "**{}**", render_span(Span::Text(t), &WikiLinks::new())?);
        }
        Span::Italic(t) => {
            write!(s, "_{}_", render_span(Span::Text(t), &WikiLinks::new())?);
        }
        Span::Image(i) => {
            let mut desc = String::new();
            if let Some(data) = i.desc {
                desc = render_span(*data, &WikiLinks::new())?;
            }
            write!(s, "![{}]({})", desc, i.url);
        }
        Span::List(_l) => {
            todo!()
        }
        Span::Preformatted(p) => {
            write!(s, "`{}`", p);
        }
        Span::Link(l) => {
            let mut desc = String::new();
            if let Some(d) = l.desc {
                desc = render_span(*d, &WikiLinks::new())?;
            }
            write!(s, "[{}]({})", desc, l.url);
        }
        Span::WikiLink(l) => {
            // TODO: implement this properly
            if let Some(l) = get_wikilink(&l, wikilinks) {
                write!(s, "{}", render_span(Span::Link(l), wikilinks)?);
            } else {
                write!(s, "[[{}]]", l);
            }
        }
    }
    Ok(s)
}

#[allow(unused_must_use)]
fn render_block(block: Block, wikilinks: &WikiLinks) -> Result<String, RenderError> {
    let mut result = String::new();
    match block {
        Block::Paragraph(t) => {
            result.push_str(&format!("{}\n", render_span(Span::Text(t), wikilinks)?));
        }
        Block::Header(h) => {
            for _ in 1..=h.level {
                result.push('#');
            }
            result.push_str(&format!(" {}\n", render_span(h.data, wikilinks)?));
        }
        Block::Image(i) => {
            let mut desc = String::new();
            if let Some(data) = i.desc {
                desc = render_span(*data, wikilinks)?;
            }
            write!(result, "![{}]({})", desc, i.url);
        }
        Block::List(l) => match l {
            List::Ordered(l) => {
                for (i, e) in l.iter().enumerate() {
                    writeln!(result, "{}. {}", i, render_span(e.clone(), wikilinks)?);
                }
            }
            List::UnOrdered(l) => {
                for e in l {
                    writeln!(result, "* {}", render_span(e.clone(), wikilinks)?);
                }
            }
        },
        Block::Quote(q) => {
            for line in q.lines() {
                result.push_str(&format!("> {}\n", line));
            }
        }
        Block::CodeBlock(c) => {
            result.write_str(&format!("```{}\n{}\n```\n", c.alt, c.data));
        }
        Block::LinkSet(vl) => {
            write!(
                result,
                "{}",
                render_block(
                    Block::List(UnOrdered(
                        vl.iter().map(|e| Span::Link(e.clone())).collect()
                    )),
                    wikilinks,
                )?
            );
        }
    };
    Ok(result)
}

/// Render a [`Document`] to a markdown string
pub fn render(doc: Document, wikilinks: &WikiLinks) -> Result<String, RenderError> {
    let mut result = String::new();
    for block in doc {
        result.push_str(&format!("{}\n", render_block(block, wikilinks)?));
    }
    if result.ends_with("\n\n") {
        result.pop();
    }
    Ok(result)
}
