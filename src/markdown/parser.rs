use log::trace;

use crate::utils::{flatten_text_span, get_header, parse_enclosed_data};
use crate::Block::Quote;
use crate::{Block, CodeBlock, Document, Header, Image, Link, List, ParseError, Span, Text};

#[cfg(test)]
mod tests {
    use log::debug;
    use pretty_assertions::assert_eq;
    use pretty_env_logger::try_init;

    use crate::markdown::parse;
    use crate::markdown::parser::parse_span;
    use crate::{Block, Span, Text};

    #[test]
    fn test_simple_paragraph() {
        let s = r#"first line
second line"#;
        let r = parse(s);
        assert!(r.is_ok());
        let doc = r.unwrap();
        assert_eq!(doc.len(), 1);
        let txt = doc.get(0);
        assert!(txt.is_some());
        let txt = txt.unwrap();
        matches!(txt, Block::Paragraph(_));
        if let Block::Paragraph(t) = txt {
            match t {
                Text::Plain(text) => assert_eq!(text, "first line second line"),
                Text::Tree(_) => panic!("Incorrect variant"),
            }
        }
    }

    #[test]
    fn test_complex_paragraph() {
        let _ = try_init();
        let s = r#"This paragraph _seems_ **fine**, but is in [reality](http://reality.dev) `complex`."#;
        let r = parse(s);
        assert!(r.is_ok());
        let doc = r.unwrap();
        assert_eq!(doc.len(), 1);
        let txt = doc.get(0);
        assert!(txt.is_some());
        let txt = txt.unwrap();
        matches!(txt, Block::Paragraph(_));
        if let Block::Paragraph(txt) = txt {
            match txt {
                Text::Plain(_) => panic!("Incorrect Variant"),
                Text::Tree(tree) => {
                    assert_eq!(tree.len(), 9);
                }
            }
        }
    }

    #[test]
    fn test_code_block() {
        let s = r##"```rust
fn main() {
    println!("hello world!");
}
```

Other text."##;
        let r = parse(s);
        assert!(r.is_ok());
        let doc = r.unwrap();
        assert_eq!(doc.len(), 2);
        let code = doc.get(0);
        assert!(code.is_some());
        let code = code.unwrap();
        matches!(code, Block::CodeBlock(_));
        if let Block::CodeBlock(code) = code {
            assert_eq!(code.alt, "rust");
            assert_eq!(
                code.data,
                r##"fn main() {
    println!("hello world!");
}
"##
            );
        }
    }

    #[test]
    fn test_full_link() {
        let _ = try_init();
        let s = "[txt](http://perdu.com)";
        let r = parse_span(0, 0, s);
        assert!(r.is_ok());
        let doc = r.unwrap();
        if let Span::Text(doc) = doc {
            if let Text::Tree(tree) = doc {
                let mut p = tree.iter();
                if let Some(link) = p.next() {
                    if let Span::Link(l) = link {
                        assert_eq!(l.url, "http://perdu.com");
                        if let Some(desc) = l.desc.clone() {
                            if let Span::Text(t) = *desc {
                                if let Text::Plain(s) = t {
                                    assert_eq!(s, "txt");
                                } else {
                                    panic!("t.data was none!");
                                }
                            } else {
                                panic!("desc was not text");
                            }
                        } else {
                            panic!("no description");
                        }
                    } else {
                        panic!("link was not a link: {:?}", link);
                    }
                }
            } else {
                panic!("not the right variant!");
            }
        } else {
            panic!("Not the right variant!");
        }
    }

    #[test]
    fn strong_text_with_underscore() {
        let _ = try_init();
        let d = parse_span(0, 0, "__strong text__");
        assert!(d.is_ok());
        let d = d.unwrap();
        debug!("d is {:#?}", d);
        if let Span::Text(t) = d {
            if let Text::Tree(t) = t {
                assert_eq!(t.len(), 1);
                let member = t.get(0).unwrap();
                if let Span::Bold(b) = member.clone() {
                    if let Text::Plain(txt) = b {
                        assert_eq!(txt, "strong text");
                    }
                } else {
                    panic!("member was not bold! {:?}", t);
                }
            }
        } else {
            panic!("d was not text: {:?}", d);
        }
    }
}

/// Attempts to break up a paragraph into a span tree.
fn parse_span<S: Into<String>>(line_no: u32, char: u32, src: S) -> Result<Span, ParseError> {
    let s = src.into();
    trace!("parse_span called with {} at {}:{}", s, line_no, char);
    // === code ===
    if let Some(start) = s.find('`') {
        // that means we have an inline code block
        trace!("found code span start at {}", start);
        let (before, code, after) = parse_enclosed_data(line_no, char, '`', s)?;
        let mut members = Vec::new();
        members.extend(flatten_text_span(parse_span(line_no, 0, before)?));
        members.push(Span::Preformatted(code.to_string()));
        members.extend(flatten_text_span(parse_span(
            line_no,
            (start + code.len()) as u32,
            after,
        )?));
        return Ok(Span::Text(Text::Tree(members)));
    }
    // === image ===
    else if let (Some(start), Some(end)) = (s.find("!["), s.find(']')) {
        if let (Some(start_link), Some(end_link)) =
            (s[end..s.len()].find('('), s[end..s.len()].find(')'))
        {
            let (before, img_desc, link, after) = (
                s[0..start].to_string(),
                s[start + 2..end].to_string(),
                s[end + start_link + 1..end + end_link].to_string(),
                s[end + end_link + 1..s.len()].to_string(),
            );
            let mut members = Vec::new();
            members.extend(flatten_text_span(parse_span(line_no, 0, before)?));
            members.push(Span::Image(Image {
                desc: Some(Box::new(Span::Text(Text::Plain(img_desc)))),
                url: link,
            }));
            members.extend(flatten_text_span(parse_span(
                line_no,
                (end + end_link + 1) as u32,
                after,
            )?));
            return Ok(Span::Text(Text::Tree(members)));
        } else {
            return Err(ParseError::new(
                line_no,
                format!("Could not find the matching () for image in {}", s),
            ));
        }
    }
    // === Wikilink ===
    else if let (Some(start), Some(end)) = (s.find("[["), s.find("]]")) {
        // that means we have a wikilink
        trace!("Found wikilink!");
        let before = &s[0..start];
        let wiki_text = &s[start + 2..end];
        let after = &s[end + 2..s.len()];
        trace!(
            "before: \"{}\", wiki_text: \"{}\", after: \"{}\"",
            before,
            wiki_text,
            after
        );
        let mut members = Vec::new();
        members.extend(flatten_text_span(parse_span(line_no, 0, before)?));
        members.push(Span::WikiLink(wiki_text.to_string()));
        members.extend(flatten_text_span(parse_span(line_no, 0, after)?));
        return Ok(Span::Text(Text::Tree(members)));
    }
    // === link ===
    else if let (Some(start), Some(end)) = (s.find('['), s.find(']')) {
        if let (Some(start_link), Some(end_link)) =
            (s[end..s.len()].find('('), s[end..s.len()].find(')'))
        {
            // then it is a complete link
            trace!(
                "found a link in {}, desc:[{},{}], link:[{},{}]",
                s,
                start,
                end,
                end + start_link + 2,
                end + end_link
            );
            let (before, link_desc, link_url, after) = (
                s[0..start].to_string(),
                s[start + 1..end].to_string(),
                s[end + start_link + 1..end + end_link].to_string(),
                s[end + end_link + 1..s.len()].to_string(),
            );
            let mut members = Vec::new();
            members.extend(flatten_text_span(parse_span(line_no, 0, before)?));
            members.push(Span::Link(Link {
                desc: Some(Box::new(parse_span(line_no, start as u32, link_desc)?)),
                url: link_url,
                key: None,
            }));
            members.extend(flatten_text_span(parse_span(
                line_no,
                (end + end_link) as u32,
                after,
            )?));
            return Ok(Span::Text(Text::Tree(members)));
        } else if let (Some(start_key), Some(end_key)) =
            (s[end..end + 2].find('['), s[end..s.len()].find(']'))
        {
            // then it has no url, but a key
            let (before, link_desc, link_key, after) = (
                s[0..start].to_string(),
                s[start + 1..end].to_string(),
                s[end + start_key + 1..end + end_key].to_string(),
                s[end + end_key + 1..s.len()].to_string(),
            );
            let mut members = Vec::new();
            members.extend(flatten_text_span(parse_span(line_no, 0, before)?));
            members.push(Span::Link(Link {
                desc: Some(Box::new(parse_span(line_no, start as u32, link_desc)?)),
                url: "".to_string(),
                key: Some(link_key),
            }));
            members.extend(flatten_text_span(parse_span(
                line_no,
                (end + end_key + 1) as u32,
                after,
            )?));
            return Ok(Span::Text(Text::Tree(members)));
        } else {
            // then it has no url, and is in itself the key.
            let (before, link_key, after) = (
                s[0..start].to_string(),
                s[start + 1..end].to_string(),
                s[end + 1..s.len()].to_string(),
            );
            let mut members = Vec::new();
            members.extend(flatten_text_span(parse_span(line_no, 0, before)?));
            members.push(Span::Link(Link {
                desc: None,
                url: "".to_string(),
                key: Some(link_key),
            }));
            members.extend(flatten_text_span(parse_span(line_no, end as u32, after)?));
            return Ok(Span::Text(Text::Tree(members)));
        }
    }
    // === emphases ===
    else if let Some(start) = s.find('*') {
        // we have found a potential emphase
        if &s[start + 1..start + 2] == "*" {
            // then we have found potential strong text
            let (before, strong, after) = parse_enclosed_data(line_no, start as u32, "**", s)?;
            let mut members = Vec::new();
            members.extend(flatten_text_span(parse_span(line_no, 0, before)?));
            members.push({
                let r = parse_span(line_no, start as u32, &strong)?;
                Span::Bold(match r {
                    Span::Text(t) | Span::Bold(t) | Span::Italic(t) => t,
                    Span::Image(i) => Text::Tree(vec![Span::Image(i)]),
                    Span::List(l) => Text::Tree(vec![Span::List(l)]),
                    Span::Preformatted(p) => Text::Tree(vec![Span::Preformatted(p)]),
                    Span::Link(l) => Text::Tree(vec![Span::Link(l)]),
                    Span::WikiLink(l) => Text::Tree(vec![Span::WikiLink(l)]),
                })
            });
            members.extend(flatten_text_span(parse_span(
                line_no,
                (start + strong.len()) as u32,
                after,
            )?));
            return Ok(Span::Text(Text::Tree(members)));
        } else {
            // try to find the next single *
            let (before, emph, after) = parse_enclosed_data(line_no, start as u32, '*', s)?;
            let mut members = Vec::new();
            members.extend(flatten_text_span(parse_span(line_no, 0, before)?));
            members.push({
                let r = parse_span(line_no, start as u32, &emph)?;
                Span::Italic(match r {
                    Span::Text(t) | Span::Bold(t) | Span::Italic(t) => t,
                    Span::Image(i) => Text::Tree(vec![Span::Image(i)]),
                    Span::List(l) => Text::Tree(vec![Span::List(l)]),
                    Span::Preformatted(p) => Text::Tree(vec![Span::Preformatted(p)]),
                    Span::Link(l) => Text::Tree(vec![Span::Link(l)]),
                    Span::WikiLink(l) => Text::Tree(vec![Span::WikiLink(l)]),
                })
            });
            members.extend(flatten_text_span(parse_span(
                line_no,
                (start + emph.len()) as u32,
                after,
            )?));
            return Ok(Span::Text(Text::Tree(members)));
        }
    } else if let Some(start) = s.find('_') {
        // we have found a potential emphase
        if &s[start + 1..start + 2] == "_" {
            // then we have found potential strong text
            let (before, strong, after) = parse_enclosed_data(line_no, start as u32, "__", s)?;
            let mut members = Vec::new();
            members.extend(flatten_text_span(parse_span(line_no, 0, before)?));
            members.push({
                let r = parse_span(line_no, start as u32, &strong)?;
                Span::Bold(match r {
                    Span::Text(t) | Span::Bold(t) | Span::Italic(t) => t,
                    Span::Image(i) => Text::Tree(vec![Span::Image(i)]),
                    Span::List(l) => Text::Tree(vec![Span::List(l)]),
                    Span::Preformatted(p) => Text::Tree(vec![Span::Preformatted(p)]),
                    Span::Link(l) => Text::Tree(vec![Span::Link(l)]),
                    Span::WikiLink(l) => Text::Tree(vec![Span::WikiLink(l)]),
                })
            });
            members.extend(flatten_text_span(parse_span(
                line_no,
                (start + strong.len()) as u32,
                after,
            )?));
            return Ok(Span::Text(Text::Tree(members)));
        } else {
            // try to find the next single _
            let (before, emph, after) = parse_enclosed_data(line_no, start as u32, '_', s)?;
            let mut members = Vec::new();
            members.extend(flatten_text_span(parse_span(line_no, 0, before)?));
            members.push({
                let r = parse_span(line_no, start as u32, &emph)?;
                Span::Italic(match r {
                    Span::Text(t) | Span::Bold(t) | Span::Italic(t) => t,
                    Span::Image(i) => Text::Tree(vec![Span::Image(i)]),
                    Span::List(l) => Text::Tree(vec![Span::List(l)]),
                    Span::Preformatted(p) => Text::Tree(vec![Span::Preformatted(p)]),
                    Span::Link(l) => Text::Tree(vec![Span::Link(l)]),
                    Span::WikiLink(l) => Text::Tree(vec![Span::WikiLink(l)]),
                })
            });
            members.extend(flatten_text_span(parse_span(
                line_no,
                (start + emph.len()) as u32,
                after,
            )?));
            return Ok(Span::Text(Text::Tree(members)));
        }
    }

    Ok(Span::Text(Text::Plain(s)))
}

#[derive(Default)]
struct ParseContext {
    is_in_code_block: bool,
    is_in_quote: bool,
    is_in_paragraph: bool,
    is_in_list: bool,
    is_ordered_list: bool,
    current_code_block: String,
    current_code_alt: String,
    current_quote: String,
    current_list: Vec<Span>,
    current_paragraph: String,
}

impl ParseContext {
    fn close_current_block(&mut self) -> Option<Block> {
        if self.is_in_list {
            Some(self.close_list())
        } else if self.is_in_code_block {
            Some(self.close_code_block())
        } else if self.is_in_quote {
            Some(self.close_quote())
        } else if self.is_in_paragraph {
            Some(self.close_paragraph())
        } else {
            None
        }
    }
    fn close_paragraph(&mut self) -> Block {
        let p = match parse_span(0, 0, self.current_paragraph.clone()) {
            Ok(p) => p,
            Err(e) => {
                panic!("Got an error: {}", e)
            }
        };
        self.current_paragraph = String::new();
        self.is_in_paragraph = false;
        if let Span::Text(t) = p {
            return Block::Paragraph(t);
        }
        panic!("parse_span didn't return a Text Span");
    }
    fn close_code_block(&mut self) -> Block {
        let code = CodeBlock {
            alt: self.current_code_alt.clone(),
            data: self.current_code_block.clone(),
        };
        self.is_in_code_block = false;
        self.current_code_block = String::new();
        self.current_code_alt = String::new();
        Block::CodeBlock(code)
    }
    fn close_quote(&mut self) -> Block {
        let quote = Quote(self.current_quote.clone());
        self.current_quote = String::new();
        self.is_in_quote = false;
        quote
    }
    fn close_list(&mut self) -> Block {
        let list = self.current_list.clone();
        self.current_list = Vec::new();
        let list = if self.is_ordered_list {
            List::Ordered(list)
        } else {
            List::UnOrdered(list)
        };
        self.is_in_list = false;
        Block::List(list)
    }
}

/// Parse a markdown string to a [`Document`]
pub fn parse<S>(src: S) -> Result<Document, ParseError>
where
    S: Into<String>,
{
    let src = src.into();
    let mut doc = Document::new();
    let mut ctx = ParseContext::default();
    for (n, line) in src.lines().enumerate() {
        if line.starts_with("```") {
            if ctx.is_in_code_block {
                doc.push(ctx.close_code_block());
            } else {
                if let Some(b) = ctx.close_current_block() {
                    doc.push(b);
                }
                ctx.is_in_code_block = true;
                ctx.current_code_alt = line.replace("```", "");
            }
        } else if ctx.is_in_code_block {
            ctx.current_code_block.push_str(&format!("{}\n", line));
        } else if line.starts_with("> ") {
            if !ctx.is_in_quote {
                if let Some(b) = ctx.close_current_block() {
                    doc.push(b);
                }
                ctx.is_in_quote = true
            }
            ctx.current_quote
                .push_str(&format!("{}\n", &line[2..line.len()]));
        } else if line.starts_with("- ") || line.starts_with("* ") {
            if !ctx.is_in_list {
                if let Some(b) = ctx.close_current_block() {
                    doc.push(b);
                }
                ctx.is_in_list = true;
                ctx.is_ordered_list = false;
            }
            ctx.current_list
                .push(parse_span(n as u32, 0, &line[2..line.len()])?);
        } else if line.starts_with('#') {
            if let Some(b) = ctx.close_current_block() {
                doc.push(b);
            }
            let (s, l) = get_header(line, 0);
            doc.push(Block::Header(Header {
                level: l,
                data: parse_span(n as u32, l as u32, s)?,
            }));
        } else if line.is_empty() {
            //TODO: close current blocks
            if let Some(b) = ctx.close_current_block() {
                doc.push(b);
            }
        }
        // TODO: handle ordered lists. they are tricky because of the number prefix
        else {
            // That means that every block has closed, and we are looking for a paragraph
            if !ctx.is_in_paragraph {
                if let Some(b) = ctx.close_current_block() {
                    doc.push(b);
                }
                ctx.is_in_paragraph = true;
            } else {
                ctx.current_paragraph.push(' ');
            }
            ctx.current_paragraph.push_str(line);
            if line.ends_with("  ") {
                ctx.current_paragraph.pop(); // then it is only a space, making the newline disappear
            }
        }
    }
    if let Some(b) = ctx.close_current_block() {
        doc.push(b);
    }
    Ok(doc)
}
