use std::path::PathBuf;

use log::trace;
#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

use crate::utils::sanitize_path;

/// Represents a link to an other ressource.
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Link {
    pub(crate) desc: Option<Box<Span>>,
    pub(crate) url: String,
    pub(crate) key: Option<String>,
}

/// Represents an image block
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Image {
    pub(crate) desc: Option<Box<Span>>,
    pub(crate) url: String,
}

/// Represents a code block. Not to be confused with [`Span::Preformatted`].
#[derive(Clone, Debug, PartialEq, Default)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct CodeBlock {
    pub(crate) alt: String,
    pub(crate) data: String,
}

/// Represents a title of some sort
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Header {
    pub(crate) level: u8,
    /// What does the header's content say?
    pub data: Span,
}

/// Some text. May be simple text, may contain some other tokens. In general, represents a paragraph.
/// NOTE: in fact, since the fields are mutually exclusive, this would make a great `enum`.
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum Text {
    /// «Pure Plain Text»®
    Plain(String),
    /// In fact, has other spans
    Tree(Vec<Span>),
}

impl Text {
    /// Create a [`Text`] from something that can be transformed to string. It won't attempt to parse the given string.
    /// ```rust
    /// use nautica::{Block, Header, Span, Text};
    /// let text = Text::from("This is a paragraph");
    /// assert_eq!(text, Text::Plain("This is a paragraph".to_string()));
    /// ```
    pub fn from<S>(s: S) -> Self
    where
        S: Into<String>,
    {
        let s = s.into();
        trace!("Text::from called with \"{}\"", s);
        Self::Plain(s)
    }
}

/// Represents a List, with its members.
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum List {
    /// Represents an ordered list, eg.
    /// ```markdown
    /// 1. an item
    /// 2. an item
    /// ```
    Ordered(Vec<Span>),
    /// Represents an unordered list, eg.
    /// ```markdown
    /// - an item
    /// - an item
    /// ```
    UnOrdered(Vec<Span>),
}

/// A Span is a sub-element of a paragraph, or block.
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum Span {
    /// represents some text
    Text(Text),
    /// represents some bold text
    Bold(Text),
    /// represents some italic text
    Italic(Text),
    /// represents an inline image
    Image(Image),
    /// Only valid in a [`List`] context, as a sub-list
    List(List),
    /// represents some preformated text, such as a struct or class name
    Preformatted(String),
    /// represents a link to an other ressource
    Link(Link),
    /// represents a wiki link. Has special handling.
    WikiLink(String),
}

/// represents a block-style element. See variants for examples
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum Block {
    /// Represents a paragraph. May contain only pure plain text or other [`Span`]s
    Paragraph(Text),
    /// Represent a header, a title
    Header(Header),
    /// Represents an image block. Not to be mistaken with [inline images][`Span::Image`]
    Image(Image),
    /// Represents a List block. See [`List`] for details.
    List(List),
    /// used for gmi rendering
    LinkSet(Vec<Link>),
    /// Represents a Quote block.
    Quote(String),
    /// Represents a code extract or sample.
    CodeBlock(CodeBlock),
}

/// A wikilink is a special type of links: They don't normally have a path associated with them, at least visually.
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct WikiLink {
    /// Text of the link
    pub text: String,
    /// Real path of the link
    pub path: PathBuf,
}

impl WikiLink {
    /// Creates a wikilink from a given text, under the form `document::whatever`.
    /// ```rust
    /// use std::path::Path;
    /// use nautica::WikiLink;
    ///
    /// let link = WikiLink::from("");
    /// assert_eq!(link.text, "root");
    /// assert_eq!(link.path, Path::new("/"));
    /// let link = WikiLink::from("Concept::Dog");
    /// assert_eq!(link.text, "Concept::Dog");
    /// assert_eq!(link.path, Path::new("/Concept/Dog"))
    /// ```
    pub fn from<S: Into<String>>(text: S) -> Self {
        let text = text.into();
        Self {
            text: {
                if text.is_empty() {
                    "root".to_string()
                } else {
                    text.to_string()
                }
            },
            path: {
                let mut path = PathBuf::new();
                for p in text.split("::") {
                    path.push(p);
                }
                if text.is_empty() {
                    PathBuf::from("/")
                } else {
                    sanitize_path(PathBuf::from(
                        "/".to_string() + &text.replace("::", "/") + "/",
                    ))
                }
            },
        }
    }
    /// returns the last element of the path/name, for potential display.
    /// ```rust
    /// use nautica::WikiLink;
    /// let link = WikiLink::from("Concept::Dog");
    /// assert_eq!(link.get_display_text(), "Dog");
    /// ```
    pub fn get_display_text(&self) -> String {
        self.text
            .rsplit("::")
            .next()
            .unwrap_or("<empty wikilink>")
            .to_string()
    }
}

/// The `Document` type is in fact an ordered list of [`Block`]s
pub type Document = Vec<Block>;
/// WikiLinks are a list of [`WikiLink`]
pub type WikiLinks = Vec<WikiLink>;
