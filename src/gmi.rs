use crate::utils::get_wikilink;
use crate::{
    Block, CodeBlock, Document, Header, Link, List, ParseError, RenderError, Span, Text, WikiLinks,
};

#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::{Read, Write};
    use std::path::PathBuf;

    use lazy_static::lazy_static;
    use pretty_assertions::assert_eq;
    use pretty_env_logger::try_init;

    use crate::gmi::{parse, render};
    use crate::{Block, Document, Link, List, Span, Text, WikiLink, WikiLinks};

    lazy_static! {
        static ref TEST_WIKILINKS_VEC: WikiLinks = vec![WikiLink {
            text: "dev::nautica::gmi::tests".to_string(),
            path: PathBuf::from("dev/nautica/gmi/tests")
        }];
    }
    #[test]
    fn parse_full_doc() {
        let p = PathBuf::from(env!("CARGO_MANIFEST_DIR"))
            .join("resources")
            .join("gemini")
            .join("test.gmi");
        println!("path: {:?}", p);
        let mut f = File::open(p).expect("Could not open test.gmi");
        let mut s = String::new();
        let _ = f.read_to_string(&mut s);
        let doc = parse(&s);
        assert!(doc.is_ok());
        let doc = doc.unwrap();
        assert!(!doc.is_empty());
        let s2 = render(doc, &TEST_WIKILINKS_VEC);
        assert!(s2.is_ok());
        let s2 = s2.unwrap();
        {
            let _ = File::create("/tmp/nautica_gmi_test_1.gmi")
                .unwrap()
                .write_all(s.as_ref());
            let _ = File::create("/tmp/nautica_gmi_test_2.gmi")
                .unwrap()
                .write_all(s2.as_ref());
        }
        assert_eq!(s, s2);
    }

    #[test]
    fn parse_text() {
        let s = vec![
            ("i'm a gemini text", "i'm a gemini text"),
            ("some text\nother", "some text other"),
        ];
        for s in s {
            let doc = parse(s.0);
            assert!(doc.is_ok());
            let doc = doc.unwrap();
            assert_eq!(doc.len(), 1);
            let txt = doc.get(0).unwrap();
            assert!(matches!(txt, Block::Paragraph(_)));
            match txt {
                Block::Paragraph(st) => match st {
                    Text::Plain(str) => {
                        assert_eq!(str, s.1);
                    }
                    Text::Tree(_) => {
                        panic!();
                    }
                },
                _ => {
                    panic!("Bad node received!");
                }
            }
        }
    }

    #[test]
    fn parse_link() {
        let good = vec![
            (
                "=> https://example.com    A cool website",
                "https://example.com",
                "A cool website",
            ),
            (
                "=> gopher://example.com   An even cooler gopherhole",
                "gopher://example.com",
                "An even cooler gopherhole",
            ),
            (
                "=> gemini://example.com   A supremely cool Gemini capsule",
                "gemini://example.com",
                "A supremely cool Gemini capsule",
            ),
            ("=> sftp://example.com", "sftp://example.com", ""),
        ];
        let good_but_weird = vec![
            (
                "=>https://example.com A cool website",
                "https://example.com",
                "A cool website",
            ),
            (
                "=>gopher://example.com      An even cooler gopherhole",
                "gopher://example.com",
                "An even cooler gopherhole",
            ),
            (
                "=> gemini://example.com A supremely cool Gemini capsule",
                "gemini://example.com",
                "A supremely cool Gemini capsule",
            ),
            ("=>   sftp://example.com", "sftp://example.com", ""),
        ];
        for l in good {
            test_link(l.0, l.1, l.2);
        }
        for l in good_but_weird {
            test_link(l.0, l.1, l.2);
        }
    }

    fn test_link(l: &str, expected_target: &str, expected_comment: &str) {
        let expected_comment = if expected_comment.is_empty() {
            None
        } else {
            Some(Box::new(Span::Text(Text::Plain(
                expected_comment.to_string(),
            ))))
        };
        let link = parse(l);
        assert!(link.is_ok());
        let link = link.unwrap();
        assert_eq!(link.len(), 1);
        let link = link.get(0).unwrap();
        assert!(matches!(link, Block::LinkSet(_)));
        let link = match link {
            Block::LinkSet(st) => st,
            _ => panic!("Invalid variant"),
        };
        let l = link.get(0).expect("could not unwrap");
        assert_eq!(l.url, expected_target);
        assert_eq!(l.desc, expected_comment);
    }

    #[test]
    fn parse_header() {
        let headers = vec![
            ("# hello", 1, "hello"),
            ("## hello", 2, "hello"),
            ("### hello world", 3, "hello world"),
        ];
        for h in headers {
            let header = parse(h.0);
            assert!(header.is_ok());
            let header = header.unwrap();
            assert_eq!(header.len(), 1);
            let header = header.get(0).unwrap();
            assert!(matches!(header, Block::Header(_)));
            match header {
                Block::Header(head) => {
                    assert_eq!(head.level, h.1);
                    assert_eq!(head.data, Span::Text(Text::Plain(h.2.to_string())));
                }
                _ => {
                    panic!("Not the expected variant");
                }
            }
        }
    }

    #[test]
    fn parse_list() {
        let list = "
* elem 1
* elem 2
";
        let list = parse(list);
        assert!(list.is_ok());
        let list = list.unwrap();
        assert_eq!(list.len(), 1);
        let list = list.get(0).unwrap();
        assert!(matches!(list, Block::List(_)));
        match list {
            Block::List(l) => match l {
                List::Ordered(_) => panic!("Invalid variant"),
                List::UnOrdered(l) => {
                    assert_eq!(l.len(), 2);
                    let l1 = l.get(0).unwrap();
                    assert_eq!(l1, &Span::Text(Text::Plain("elem 1".to_string())));
                    let l2 = l.get(1).unwrap();
                    assert_eq!(l2, &Span::Text(Text::Plain("elem 2".to_string())));
                }
            },
            _ => {
                panic!("not the expected variant")
            }
        }
    }

    #[test]
    fn parse_blockquote() {
        let quote_s = (
            "> All that is sacred isn't gold",
            "All that is sacred isn't gold",
        );
        let quote = parse(quote_s.0);
        assert!(quote.is_ok());
        let quote = quote.unwrap();
        assert_eq!(quote.len(), 1);
        let quote = quote.get(0).unwrap();
        assert!(matches!(quote, Block::Quote(_)));
        match quote {
            Block::Quote(q) => assert_eq!(q, quote_s.1),
            _ => panic!("not the expected variant"),
        }
    }

    #[test]
    fn parse_preformatted() {
        let texts = vec![
            (
                "```
hello
```",
                "hello\n",
                "",
            ),
            (
                "```text
hello
```",
                "hello\n",
                "text",
            ),
        ];
        for text in texts {
            let t = parse(text.0);
            assert!(t.is_ok());
            let t = t.unwrap();
            assert_eq!(t.len(), 1);
            let t = t.get(0).unwrap();
            assert!(matches!(t, Block::CodeBlock(_)));
            match t {
                Block::CodeBlock(p) => {
                    assert_eq!(p.data, text.1);
                    assert_eq!(p.alt, text.2);
                }
                _ => panic!("unexpected node variant"),
            }
        }
    }

    #[test]
    fn render_link() {
        let _ = try_init();
        let expected = r#"here is a link[1].

=> test.url [1]
"#;
        let d: Document = vec![Block::Paragraph(Text::Tree(vec![
            Span::Text(Text::Plain("here is a ".to_string())),
            Span::Link(Link {
                desc: Some(Box::new(Span::Text(Text::Plain("link".to_string())))),
                url: "test.url".to_string(),
                key: None,
            }),
            Span::Text(Text::Plain(".".to_string())),
        ]))];
        let s = render(d, &Vec::new());
        assert!(s.is_ok());
        let s = s.unwrap();
        println!("exp: {}", expected);
        println!("got: {}", s);
        assert_eq!(expected, s);
    }

    #[test]
    fn render_links() {
        let _ = try_init();
        let expected = r#"here is a link[1]. And an other[2].

=> test.url [1]
=> test2.url [2]

And an other paragraph.
"#;
        let d: Document = vec![
            Block::Paragraph(Text::Tree(vec![
                Span::Text(Text::Plain("here is a ".to_string())),
                Span::Link(Link {
                    desc: Some(Box::new(Span::Text(Text::Plain("link".to_string())))),
                    url: "test.url".to_string(),
                    key: None,
                }),
                Span::Text(Text::Plain(". And an ".to_string())),
                Span::Link(Link {
                    desc: Some(Box::new(Span::Text(Text::Plain("other".to_string())))),
                    url: "test2.url".to_string(),
                    key: None,
                }),
                Span::Text(Text::Plain(".".to_string())),
            ])),
            Block::Paragraph(Text::Tree(vec![Span::Text(Text::Plain(
                "And an other paragraph.".to_string(),
            ))])),
        ];
        let s = render(d, &Vec::new());
        assert!(s.is_ok());
        let s = s.unwrap();
        println!("exp: {}", expected);
        println!("got: {}", s);
        assert_eq!(expected, s);
    }
}

#[derive(Debug, Clone, Default)]
struct GeminiParsingContext {
    is_in_preformatted_block: bool,
    previous_line_was_list: bool,
    previous_line_was_text: bool,
    current_text: String,
    current_list: Vec<String>,
    current_alt: String,
}

/// Parse a gemtext string to a [`Document`]
pub fn parse<S: Into<String>>(src: S) -> Result<Document, ParseError> {
    let src: String = src.into();
    let mut context = GeminiParsingContext::default();
    let mut doc = Document::new();
    for (n, line) in src.lines().enumerate() {
        if !line.starts_with("* ")
            && context.previous_line_was_list
            && !context.is_in_preformatted_block
        {
            context.previous_line_was_list = false;
            let l = context.current_list;
            context.current_list = Vec::new();
            doc.push(Block::List(List::UnOrdered(
                l.iter().map(|s| Span::Text(Text::from(s))).collect(),
            )));
        }
        if line.starts_with("> ") && !context.is_in_preformatted_block {
            // then it is a blockquote
            doc.push(Block::Quote(line.replace("> ", "")));
        } else if line.starts_with("* ") && !context.is_in_preformatted_block {
            //then it is a list
            if !context.previous_line_was_list {
                context.current_list = Vec::new();
            }
            context.previous_line_was_list = true;
            context.current_list.push(line.replace("* ", ""));
        } else if line.starts_with("=>") && !context.is_in_preformatted_block {
            // then it is a link
            let line = line.replace("=>", "");
            let mut l = line.split_whitespace();
            let link = match l.next() {
                None => {
                    return Err(ParseError::new(
                        n as u32,
                        "could not construct link: no url",
                    ));
                }
                Some(s) => s,
            };
            let comment: String = l.map(|s| s.to_string()).collect::<Vec<String>>().join(" ");
            let comment_opt = match comment.is_empty() {
                true => None,
                false => Some(Box::new(Span::Text(Text::from(&comment)))),
            };
            doc.push(Block::LinkSet(vec![Link {
                url: String::from(link),
                desc: comment_opt,
                key: None,
            }]));
        } else if line.starts_with("# ") && !context.is_in_preformatted_block {
            doc.push(Block::Header(Header {
                level: 1,
                data: Span::Text(Text::from(&line.replace("# ", ""))),
            }));
        } else if line.starts_with("## ") && !context.is_in_preformatted_block {
            doc.push(Block::Header(Header {
                level: 2,
                data: Span::Text(Text::from(&line.replace("## ", ""))),
            }));
        } else if line.starts_with("### ") && !context.is_in_preformatted_block {
            doc.push(Block::Header(Header {
                level: 3,
                data: Span::Text(Text::from(&line.replace("### ", ""))),
            }));
        } else if line.starts_with("```") {
            if context.is_in_preformatted_block {
                // then it is over and we need to append the node to the list
                doc.push(Block::CodeBlock(CodeBlock {
                    alt: context.current_alt.clone(),
                    data: context.current_text.clone(),
                }));
                context.current_text = String::new();
                context.current_alt = String::new();
                context.is_in_preformatted_block = false;
            } else {
                context.is_in_preformatted_block = true;
                context.current_alt = line.replace("```", "");
            }
        } else {
            if context.is_in_preformatted_block {
                context.current_text.push_str(line);
                context.current_text.push('\n');
                continue;
            }
            // it is simple text
            if line.is_empty() {
                if !context.current_text.is_empty() {
                    doc.push(Block::Paragraph(Text::from(&context.current_text)));
                    context.current_text = String::new();
                }
                context.previous_line_was_text = false;
            } else {
                if context.previous_line_was_text {
                    context.current_text.push(' ');
                }
                context.current_text.push_str(line);
                context.previous_line_was_text = true;
            }
        }
    }
    if !context.current_list.is_empty() {
        doc.push(Block::List(List::UnOrdered(
            context
                .current_list
                .iter()
                .map(|s| Span::Text(Text::from(s)))
                .collect(),
        )));
    }
    if !context.current_text.is_empty() {
        doc.push(Block::Paragraph(Text::from(&context.current_text)));
    }
    Ok(doc)
}

/// # returns
/// the rendered string, a collection of blocks to add after the current block
fn render_span(
    span: Span,
    link_no: &mut u32,
    wikilinks: &WikiLinks,
) -> Result<(String, Vec<Block>), RenderError> {
    let mut result = String::new();
    let mut extra_blocks = Vec::new();
    let mut links = Vec::new();
    match span {
        Span::Text(t) | Span::Bold(t) | Span::Italic(t) => {
            match t {
                Text::Plain(s) => {
                    result.push_str(&s);
                }
                Text::Tree(t) => {
                    let mut s = String::new();
                    for span in t {
                        // TODO: raise error on any span variant other than text::pure
                        let span = &render_span(span, link_no, wikilinks)?;
                        s.push_str(&span.0);
                        extra_blocks.extend(span.clone().1);
                    }
                    result.push_str(&s);
                }
            }
        }
        Span::Image(i) => {
            if let Some(desc) = i.desc {
                result.push_str(&format!(
                    "=> {} {}\n",
                    i.url,
                    render_span(*desc, link_no, wikilinks)?.0
                ));
            } else {
                result.push_str(&format!("=> {} [{}]\n", i.url, link_no));
                *link_no += 1;
            }
        }
        Span::List(list) => match list {
            List::Ordered(l) | List::UnOrdered(l) => {
                for e in l {
                    match e {
                        Span::Text(t) | Span::Bold(t) | Span::Italic(t) => {
                            if let Text::Plain(s) = t {
                                result.push_str(&s);
                            } else {
                                return Err(RenderError::new(
                                    "Can only render simple text inside a list",
                                    Span::Text(t),
                                ));
                            }
                        }
                        _ => {
                            return Err(RenderError::new(
                                "Can only render simple text inside a list",
                                e,
                            ));
                        }
                    };
                }
            }
        },
        Span::Preformatted(pre) => result.push_str(&pre),
        Span::Link(l) => {
            if let Some(desc) = l.desc {
                links.push(Link {
                    desc: Some(Box::new(Span::Text(Text::Plain(format!("[{}]", link_no))))),
                    url: l.url.clone(),
                    key: None,
                });
                result.push_str(&format!(
                    "{}[{}]",
                    render_span(*desc, link_no, wikilinks)?.0,
                    link_no
                ));
            } else {
                links.push(Link {
                    desc: Some(Box::new(Span::Text(Text::Plain(format!("[{}]", link_no))))),
                    url: l.url.clone(),
                    key: None,
                });
                result.push_str(&format!("[{}]", link_no));
            }
            *link_no += 1;
        }
        Span::WikiLink(wikilink) => {
            result.push_str(&format!("[[{}]]", wikilink));
            if let Some(l) = get_wikilink(wikilink, wikilinks) {
                links.push(l);
            }
        }
    };
    if !links.is_empty() {
        extra_blocks.push(Block::LinkSet(links));
    }
    Ok((result, extra_blocks))
}

fn render_block(
    block: Block,
    link_no: &mut u32,
    wikilinks: &WikiLinks,
) -> Result<(String, Vec<Block>), RenderError> {
    let mut result = String::new();
    let mut extra = Vec::new();
    let mut links = Vec::new();
    match block {
        Block::Paragraph(t) => {
            // discard if not a pure paragraph
            match t {
                Text::Plain(s) => {
                    result.push_str(&format!("{}\n", s));
                }
                Text::Tree(t) => {
                    for span in t {
                        let sp = render_span(span, link_no, wikilinks)?;
                        result.push_str(&sp.0.to_string());
                        // TODO: merge blocks?
                        for b in sp.1 {
                            if let Block::LinkSet(vl) = b {
                                links.extend(vl);
                            } else {
                                extra.push(b);
                            }
                        }
                    }
                    result.push('\n');
                }
            }
        }
        Block::Header(h) => {
            let level = match h.level {
                0 => {
                    return Err(RenderError::new("Header level is 0!", Block::Header(h)));
                }
                1..=3 => h.level,
                _ => {
                    return Err(RenderError::new(
                        "Header level too big for gemini parser!",
                        Block::Header(h),
                    ));
                }
            };
            for _i in 1..=level {
                result.push('#');
            }
            let sp = render_span(h.data, link_no, wikilinks)?;
            result.push_str(&format!(" {}\n", sp.0));
        }
        Block::List(l) => match l {
            List::Ordered(l) | List::UnOrdered(l) => {
                for e in l {
                    let sp = render_span(e, link_no, wikilinks)?;
                    result.push_str(&format!("* {}\n", sp.0));
                    extra.extend(sp.1);
                }
            }
        },
        Block::Quote(q) => {
            result.push_str(&format!("> {}\n", q));
        }
        Block::CodeBlock(p) => {
            result.push_str(&format!("```{}\n{}```\n", p.alt, p.data));
        }
        Block::Image(i) => {
            let sp = render_span(Span::Image(i), link_no, wikilinks)?;
            result.push_str(&format!("{}\n", sp.0));
            extra.extend(sp.1);
        }
        Block::LinkSet(vl) => {
            for l in vl {
                if let Some(desc) = l.desc {
                    result.push_str(&format!(
                        "=> {} {}\n",
                        l.url,
                        render_span(*desc, link_no, wikilinks)?.0
                    ));
                } else {
                    // should never happen, but we never know, right?
                    result.push_str(&format!("=> {}\n", l.url));
                }
            }
        }
    }
    if !links.is_empty() {
        extra.push(Block::LinkSet(links));
    }
    Ok((result, extra))
}

/// Render a [`Document`] to a string, taking into account a given set of [`crate::document::WikiLink`].
pub fn render(doc: Document, wikilinks: &WikiLinks) -> Result<String, RenderError> {
    let mut result = String::new();
    let mut link_no = 1;
    for block in doc {
        let sp = render_block(block, &mut link_no, wikilinks)?;
        result.push_str(&format!("{}\n", sp.0));
        for block in sp.1 {
            let sp = render_block(block, &mut link_no, wikilinks)?;
            result.push_str(&format!("{}\n", sp.0));
        }
    }
    // Remove last \n
    let _ = result.pop();
    Ok(result)
}
