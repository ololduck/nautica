use crate::List::UnOrdered;
use crate::{
    Block, CodeBlock, Document, Image, List, RenderError, Span, Text, WikiLink, WikiLinks,
};
use log::trace;
use syntect::highlighting::ThemeSet;
use syntect::html::highlighted_html_for_string;
use syntect::parsing::SyntaxSet;

#[cfg(test)]
mod tests {
    use lazy_static::lazy_static;
    use pretty_assertions::assert_eq;
    use std::fs::File;
    use std::io::{Read, Write};
    use std::path::PathBuf;

    use crate::html::{highlight_syntax, render, render_block, render_span};
    use crate::{Block, CodeBlock, Link, Span, Text, WikiLink, WikiLinks};

    lazy_static! {
        static ref TEST_WIKILINKS_VEC: WikiLinks = vec![WikiLink {
            text: "dev::nautica::tests".to_string(),
            path: PathBuf::from("dev/nautica/tests")
        }];
    }

    fn run_span(r: Vec<(Span, &str)>) {
        for (span, expected) in r {
            println!("expecting \"{}\" for {:?}…", expected, span);
            let s = render_span(span, &TEST_WIKILINKS_VEC);
            assert!(s.is_ok());
            let s = s.unwrap();
            assert_eq!(s, expected.to_string());
        }
    }

    #[test]
    fn test_span_text() {
        let r = vec![
            (
                Span::Text(Text::Plain("Simple Text".to_string())),
                "Simple Text",
            ),
            (
                Span::Text(Text::Tree(vec![
                    Span::Text(Text::Plain("Hello, ".to_string())),
                    Span::Bold(Text::Plain("World".to_string())),
                    Span::Text(Text::Plain(".".to_string())),
                ])),
                "Hello, <strong>World</strong>.",
            ),
        ];
        run_span(r);
    }

    #[test]
    fn test_span_link() {
        let r = vec![
            (
                Span::Text(Text::Tree(vec![
                    Span::Text(Text::Plain("Hello, ".to_string())),
                    Span::Link(Link {
                        desc: None,
                        url: "https://goelands.vit.am/".to_string(),
                        key: None,
                    }),
                    Span::Text(Text::Plain(".".to_string())),
                ])),
                "Hello, <a href=\"https://goelands.vit.am/\">https://goelands.vit.am/</a>.",
            ),
            (
                Span::Text(Text::Tree(vec![
                    Span::Text(Text::Plain("Hello, ".to_string())),
                    Span::Link(Link {
                        desc: Some(Box::new(Span::Text(Text::Plain("description".to_string())))),
                        url: "https://goelands.vit.am/".to_string(),
                        key: None,
                    }),
                    Span::Text(Text::Plain(".".to_string())),
                ])),
                "Hello, <a href=\"https://goelands.vit.am/\">description</a>.",
            ),
        ];
        run_span(r);
    }

    #[test]
    fn highlight() {
        let r = CodeBlock {
            alt: "rust".to_string(),
            data: "pub fn main() {println!(\"Hello, world!\");}".to_string(),
        };
        let r = highlight_syntax(r);
        let r = render_block(Block::CodeBlock(r), &TEST_WIKILINKS_VEC);
        assert!(r.is_ok());
    }

    #[test]
    fn full() {
        let p = PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("resources");
        println!("path: {:?}", p);
        let mut f = File::open(p.join("gemini").join("test.gmi")).expect("Could not open test.gmi");
        let mut s = String::new();
        let _ = f.read_to_string(&mut s);
        let doc = crate::gmi::parse(&s);
        assert!(doc.is_ok());
        let doc = doc.unwrap();
        assert!(!doc.is_empty());
        println!("{:?}", doc);
        let s = render(doc, &TEST_WIKILINKS_VEC);
        assert!(s.is_ok());
        let s = s.unwrap();
        println!("{}", s);
        let _ = File::create("/tmp/gmi_to_html.html")
            .unwrap()
            .write_all(s.as_ref());
        let mut f = File::open(p.join("gmi_to_html.html")).unwrap();
        let mut s2 = String::new();
        let _ = f.read_to_string(&mut s2);
        assert_eq!(s, s2);
    }
}

/// Attempt to render a given [`Document`] to an html `<article>`.
pub fn render(doc: Document, wikilinks: &WikiLinks) -> Result<String, RenderError> {
    let mut s = String::new();
    for block in doc {
        s.push_str(&render_block(block, wikilinks)?);
    }
    Ok(format!("<article>{}</article>", s))
}

fn render_block(block: Block, wikilinks: &WikiLinks) -> Result<String, RenderError> {
    Ok(match block {
        Block::Paragraph(t) => {
            format!("<p>{}</p>", {
                match t {
                    Text::Plain(text) => html_escape(&text),
                    Text::Tree(tree) => {
                        let mut s = String::new();
                        for e in tree {
                            s.push_str(&render_span(e, wikilinks)?);
                        }
                        s
                    }
                }
            })
        }
        Block::Header(h) => {
            if h.level > 6 {
                return Err(RenderError::new("invalid header level", Block::Header(h)));
            }
            format!("<h{0}>{1}</h{0}>", h.level, render_span(h.data, wikilinks)?)
        }
        Block::Image(i) => {
            // TODO: add alt=i.desc
            format!("<img src=\"{}\" />", i.url)
        }
        Block::List(l) => render_list(l, wikilinks)?,
        Block::Quote(q) => {
            format!("<blockquote>{}</blockquote>", q)
        }
        Block::CodeBlock(c) => {
            let mut c = c;
            if c.alt.is_empty() {
                c.alt = "text".to_string()
            }
            let highlighted = highlight_syntax(c);
            format!("<pre>{}</pre>", highlighted.data)
        }
        Block::LinkSet(vl) => render_block(
            Block::List(UnOrdered(
                vl.iter().map(|e| Span::Link(e.clone())).collect(),
            )),
            wikilinks,
        )?,
    })
}

fn render_span(span: Span, wikilinks: &WikiLinks) -> Result<String, RenderError> {
    Ok(match span {
        Span::Text(t) => {
            return match t {
                Text::Plain(text) => Ok(html_escape(&text)),
                Text::Tree(tree) => {
                    let mut s = String::new();
                    for e in tree {
                        s.push_str(&render_span(e, wikilinks)?);
                    }
                    Ok(s)
                }
            }
        }
        Span::Bold(b) => {
            format!(
                "<strong>{}</strong>",
                render_span(Span::Text(b), wikilinks)?
            )
        }
        Span::Italic(i) => {
            format!("<em>{}</em>", render_span(Span::Text(i), wikilinks)?)
        }
        Span::Image(i) => {
            format!(
                "<img style=\"display: inline\" alt=\"\" src=\"{}\" />",
                i.url
            )
        }
        Span::List(l) => render_list(l, wikilinks)?,
        Span::Preformatted(c) => {
            format!("<code>{}</code>", c)
        }
        Span::Link(l) => {
            for p in &[".png", ".jpg", ".gif"] {
                if l.url.ends_with(p) {
                    trace!("found image link, checking if we need to render it inline…");
                    if let Some(t) = l.desc.clone() {
                        if let Span::Text(t) = *t {
                            if let Text::Plain(s) = t {
                                if s.ends_with("[inline]") {
                                    trace!("found [inline] tag, rendering it.");
                                    return render_block(
                                        Block::Image(Image {
                                            desc: Some(Box::new(Span::Text(Text::Plain(
                                                s.replace("[inline]", ""),
                                            )))),
                                            url: l.url,
                                        }),
                                        wikilinks,
                                    );
                                }
                            }
                        };
                    }
                }
            }
            match l.url.contains("://") {
                true => format!(
                    "<a href=\"{}\">{}</a>",
                    &l.url,
                    match l.desc.clone() {
                        Some(desc) => {
                            render_span(*desc, wikilinks)?
                        }
                        None => l.url.clone(),
                    }
                ),
                false => format!(
                    "<a class=\"internal\" href=\"{}\">{}</a>",
                    &l.url,
                    match l.desc.clone() {
                        Some(desc) => {
                            render_span(*desc, wikilinks)?
                        }
                        None => l.url.clone(),
                    }
                ),
            }
        }
        Span::WikiLink(wikilink) => match wikilinks.iter().find(|e| e.text == wikilink) {
            None => {
                let link = WikiLink::from(wikilink);
                format!(
                    "<a class=\"internal, wikilink, deadlink\" href=\"{}\">{}</a>",
                    { link.path.display().to_string() },
                    link.get_display_text()
                )
            }
            Some(wikilink) => {
                format!(
                    "<a class=\"internal, wikilink\" href=\"{}\">{}</a>",
                    { wikilink.path.display().to_string() },
                    wikilink.get_display_text()
                )
            }
        },
    })
}

fn highlight_syntax(block: CodeBlock) -> CodeBlock {
    let syntax_set = SyntaxSet::load_defaults_newlines();
    let syntax = match syntax_set.find_syntax_by_name(&block.alt) {
        None => {
            return block;
        }
        Some(s) => s,
    };
    let theme_set = ThemeSet::load_defaults();
    CodeBlock {
        alt: block.alt,
        data: highlighted_html_for_string(
            &block.data,
            &syntax_set,
            syntax,
            theme_set
                .themes
                .get("base16-ocean.dark")
                .expect("could not find theme base16-ocean.dark"),
        ),
    }
}
fn html_escape(s: &str) -> String {
    s.replace('<', "&lt;").replace('>', "&gt;")
}

fn render_list(l: List, wikilinks: &WikiLinks) -> Result<String, RenderError> {
    Ok(match l {
        List::Ordered(l) => {
            let mut s = String::new();
            for e in l {
                s.push_str(&format!("<li>{}</li>", render_span(e, wikilinks)?))
            }
            format!("<ol>{}</ol>", s)
        }
        UnOrdered(l) => {
            let mut s = String::new();
            for e in l {
                s.push_str(&format!("<li>{}</li>", render_span(e, wikilinks)?))
            }
            format!("<ul>{}</ul>", s)
        }
    })
}
