#![deny(rustdoc::broken_intra_doc_links)]
#![deny(missing_docs)]
#![deny(rustdoc::missing_crate_level_docs)]
#![deny(rustdoc::invalid_codeblock_attributes)]
//! # Ṇautica
//!
//! Ṇautica is a parser. She transforms markdown and [gemtext] into a tree of tokens. This can then be used to output html,
//! gemtext, or markdown.
//!
//! At this time, she can handle these input formats:
//!
//! - [x] [gemtext]
//! - [x] markdown
//!
//! And output these:
//!
//! - [x] gemtext
//! - [x] markdown
//! - [x] html5
//!
//! ## Usage
//!
//! ### Cargo.toml
//!
//! ```toml
//! [dependencies]
//! nautica = { git = "https://framagit.org/ololduck/nautica.git", features = ["serde"] }
//! ```
//!
//! ### Code
//!
//! All parsers are in their own rust submodule, with a `render` & a `parse` method.
//!
//! ```rust
//! use nautica::{gmi, markdown};
//!
//! let md = markdown::render(gmi::parse("# a gemtext document").unwrap(), &vec![]).unwrap();
//! assert_eq!(md, "# a gemtext document\n");
//! ```
//!
//! If you want to render a markdown file both to gmi & html:
//! ```rust
//! use nautica::{gmi, html, markdown};
//!
//! let doc = markdown::parse("# my markdown").unwrap();
//! let gmi = gmi::render(doc.clone(), &vec![]).unwrap();
//! let html = html::render(doc, &vec![]).unwrap();
//! assert_eq!(gmi, "# my markdown\n");
//! assert_eq!(html, "<article><h1>my markdown</h1></article>");
//! ```
//!
//! You may have noticed that the `render` functions always have a second parameter: this is to give it a WikiLinks array so it may know what to do with them. See `nautica::WikiLink` in [document.rs](src/document.rs).
//!
//! ## TODO
//!
//! - [ ] Handle deported links, such as the `[gemtext]` in this README.
//!
//! [gemtext]: https://gemini.circumlunar.space/docs/gemtext.gm
use std::error::Error;
use std::fmt;
use std::fmt::Formatter;

pub use document::*;

/// Structures pertaining to document structures
pub mod document;
/// Functions related to gemtext handling
pub mod gmi;
/// Functions related to html handling
pub mod html;
/// Functions related to markdown handling
pub mod markdown;
/// Useful stuff
pub mod utils;

/// This is returned by all `parse` methods, to explain where and why we could not build a [`Document`] from the given text.
#[derive(Debug, Clone)]
pub struct ParseError {
    line: u32,
    msg: String,
}
impl Error for ParseError {}

#[derive(Debug, Clone)]
enum ErrorContext {
    BlockContext(Block),
    SpanContext(Span),
}

/// This is returned by all `render` methods, to explain why we could not convert the [`Document`] to the chosen format.
#[derive(Debug, Clone)]
pub struct RenderError {
    msg: String,
    context_element: Option<ErrorContext>,
}
impl Error for RenderError {}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Could not parse text at line {}:{}", self.line, self.msg)
    }
}

impl fmt::Display for RenderError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        if let Some(e) = self.clone().context_element {
            write!(f, "Could not render as gemini: {} at {:?}", self.msg, e)
        } else {
            write!(f, "could not render as gemini: {}", self.msg)
        }
    }
}

trait DocumentElem {
    fn to_error_context(self) -> ErrorContext;
}

impl DocumentElem for Block {
    fn to_error_context(self) -> ErrorContext {
        ErrorContext::BlockContext(self)
    }
}
impl DocumentElem for Span {
    fn to_error_context(self) -> ErrorContext {
        ErrorContext::SpanContext(self)
    }
}

impl ParseError {
    fn new<S: Into<String>>(line: u32, msg: S) -> ParseError {
        ParseError {
            line,
            msg: msg.into(),
        }
    }
}

impl RenderError {
    fn new<T>(msg: &str, context_elem: T) -> Self
    where
        T: DocumentElem,
    {
        Self {
            msg: msg.to_string(),
            context_element: Some(context_elem.to_error_context()),
        }
    }
}
