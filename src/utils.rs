use std::path::PathBuf;

use log::trace;

use crate::{Block, Document, Link, List, ParseError, Span, Text, WikiLink, WikiLinks};

#[cfg(test)]
mod tests {
    use std::path::PathBuf;

    use log::debug;
    use pretty_assertions::assert_eq;
    use pretty_env_logger::try_init;
    use test_case::test_case;

    use crate::utils::{flatten_text_span, get_header, sanitize_path};
    use crate::{Span, Text};

    #[test_case("# level1", ("level1", 1); "level 1")]
    #[test_case("### level 3 ", ("level 3", 3); "level 3")]
    #[test_case("#############l", ("l", 13); "moult level")]
    fn test_header(s: &str, expected: (&str, u8)) {
        let (s, l) = get_header(s, 0);
        self::assert_eq!(s, expected.0);
        self::assert_eq!(l, expected.1);
    }

    #[test]
    fn test_flatten() {
        let _ = try_init();
        let span = Span::Text(Text::Tree(vec![Span::Text(Text::Tree(vec![
            Span::Text(Text::Plain("before".to_string())),
            Span::Text(Text::Plain("".to_string())),
            Span::Text(Text::Tree(vec![Span::Bold(Text::Plain(
                "bold text".to_string(),
            ))])),
            Span::Text(Text::Plain("after".to_string())),
        ]))]));
        debug!("{:#?}", span);
        let s = flatten_text_span(span);
        assert_eq!(s.len(), 3);
    }

    #[test]
    fn sanitize() {
        let p = PathBuf::from("hello/héloïse/à/paris");
        assert_eq!(sanitize_path(p), PathBuf::from("hello/heloise/a/paris"));
    }
}

const REPLACE_CHARS: &[(&[char], char)] = &[
    (&['à'], 'a'),
    (&['é', 'è'], 'e'),
    (&['ï'], 'i'),
    (&['Ṇ'], 'N'),
];

pub(crate) fn get_wikilink<S: Into<String>>(wikilink: S, wikilinks: &WikiLinks) -> Option<Link> {
    let wikilink = wikilink.into();
    wikilinks
        .iter()
        .find(|e| e.text == wikilink)
        .map(|wikilink| Link {
            desc: Some(Box::new(Span::Text(Text::Plain(format!(
                "[[{}]]",
                wikilink.text
            ))))),
            url: wikilink.path.display().to_string(),
            key: None,
        })
}

/// flattens a [Span::Text] tree into a list
pub(crate) fn flatten_text_span(span: Span) -> Vec<Span> {
    trace!("flatten_text_span called with {:?}", span);
    let mut v = Vec::new();
    if let Span::Text(t) = span.clone() {
        match t {
            Text::Plain(s) => {
                if !s.is_empty() {
                    v.push(span);
                }
            }
            Text::Tree(t) => {
                for e in t {
                    v.extend(flatten_text_span(e));
                }
            }
        }
    } else {
        v.push(span);
    }
    trace!("flatten_text_span: {:?}", v);
    v
}

/// Returns stuff between two tokens, such as '`' or "**"
/// # Returns
/// (before, inside, after)
pub(crate) fn parse_enclosed_data<S: Into<String>, T: Into<String>>(
    line_no: u32,
    char: u32,
    needle: S,
    haystack: T,
) -> Result<(String, String, String), ParseError> {
    let needle = needle.into();
    let s = haystack.into();
    trace!(
        "parse_enclosed data called at {}:{}, looking for {} in {}.",
        line_no,
        char,
        needle,
        s
    );
    if let Some(start) = s.find(&needle) {
        // that means we have an inline code block
        trace!("found needle start at {}", start);
        let end = start
            + needle.len()
            + match s[start + needle.len()..s.len()].find(&needle) {
                None => {
                    return Err(ParseError::new(
                        line_no,
                        format!("could not find ending \"{}\"!", &needle),
                    ));
                }
                Some(pos) => pos,
            };
        trace!("found needle end at {}", end);
        let before = &s[0..start];
        let inside = &s[start + needle.len()..end];
        let after = &s[end + needle.len()..s.len()];
        trace!(
            "before: \"{}\", inside: \"{}\", after: \"{}\"",
            before,
            inside,
            after
        );
        return Ok((before.to_string(), inside.to_string(), after.to_string()));
    }
    Err(ParseError::new(line_no, "Could not find starting needle"))
}

/// Returns the level of a header & its string. Tests are ignored because i'm not sure i want to set `get_header` as truly public.
/// # Examples
/// ```rust,ignore
/// let (s,l) = get_header("# title", 0);
/// assert_eq!(s, "title");
/// assert_eq!(l, 1);
/// ```
/// ```rust,ignore
/// let (s, l) = get_header("#### other title ", 0);
/// assert_eq!(s, "other title");
/// assert_eq!(l, 4);
/// ```
pub(crate) fn get_header<S: Into<String>>(line: S, depth: u8) -> (String, u8) {
    let line = line.into();
    if line.starts_with('#') {
        get_header(&line[1..line.len()], depth + 1)
    } else if line.starts_with(' ') {
        get_header(&line[1..line.len()], depth)
    } else if line.ends_with(' ') {
        get_header(&line[0..line.len() - 1], depth)
    } else {
        (line, depth)
    }
}

fn replace_chars(s: &str) -> String {
    let s = s
        .chars()
        .map(|c| {
            for (chars, replacement) in REPLACE_CHARS {
                if chars.contains(&c) {
                    return *replacement;
                }
            }
            c
        })
        .collect();
    s
}

/// Replaces non-ascii chars in path
pub fn sanitize_path<S: Into<PathBuf>>(path: S) -> PathBuf {
    let path = path.into();
    PathBuf::from(replace_chars(&format!("{}", path.display())))
}

/// This will attempt to find all items of type [`WikiLink`] in a given [`Document`].
pub fn find_wikilinks_in_doc(doc: &Document) -> WikiLinks {
    let mut r = WikiLinks::new();
    for block in doc {
        let res = match block {
            Block::Paragraph(p) => find_wikilinks_in_span(&Span::Text(p.clone())),
            Block::Header(h) => find_wikilinks_in_span(&h.data),
            Block::List(l) => find_wikilinks_in_list(l),
            _ => WikiLinks::new(),
        };
        r.extend(res);
    }
    r
}

fn find_wikilinks_in_list(l: &List) -> WikiLinks {
    match l {
        List::Ordered(ol) => {
            let mut v = WikiLinks::new();
            for e in ol {
                v.extend(find_wikilinks_in_span(e));
            }
            v
        }
        List::UnOrdered(ul) => {
            let mut v = WikiLinks::new();
            for e in ul {
                v.extend(find_wikilinks_in_span(e));
            }
            v
        }
    }
}

fn find_wikilinks_in_span(span: &Span) -> WikiLinks {
    let mut r = WikiLinks::new();
    match span {
        Span::Text(t) | Span::Italic(t) | Span::Bold(t) => {
            if let Text::Tree(t) = t {
                for e in t {
                    r.extend(find_wikilinks_in_span(e));
                }
            }
        }
        Span::Image(i) => {
            if let Some(desc) = &i.desc {
                r.extend(find_wikilinks_in_span(desc));
            }
        }
        Span::List(l) => {
            r.extend(find_wikilinks_in_list(l));
        }
        Span::WikiLink(w) => {
            r.push(WikiLink::from(w));
        }
        _ => {}
    }
    r
}
